from django.conf.urls import patterns, url

from .views import Home

urlpatterns = patterns ('webbnc.homepage.views',
    url(r'^$', Home.as_view(), name='home'),
)