from django.conf.urls import include
from django.conf.urls import patterns
from django.conf.urls import url
from django.views.generic.base import TemplateView

from django.contrib.auth import views as auth_views
import django.contrib.auth.urls

from .views import RegistrationView

urlpatterns = patterns('',
    url(r'^register/$', RegistrationView.as_view(), 
      name='registration_register'),
    url(r'^register/closed/$',
        TemplateView.as_view(template_name='registration_closed.html'),
        name='registration_disallowed'),
    url(r'^logout/$',
       auth_views.logout,
       {'template_name': 'registration/logout.html',
        'next_page': '/'},
       name='auth_logout'),
    url(r'^login/$',
       auth_views.login,
       {'template_name': 'registration/login.html',
        },
       name='auth_login'),
    (r'', include(django.contrib.auth.urls)),
    )