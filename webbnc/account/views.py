from django.views.generic.edit import FormView

from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.conf import settings
from django.shortcuts import redirect

from .forms import UserCreationForm
from .models import MyUser

class RegistrationView(FormView):
    """
    A registration backend which implements the simplest possible
    workflow: a user supplies a username, email address and password
    (the bare minimum for a useful account), and is immediately signed
    up and logged in).
    
    """

    disallowed_url = 'registration_closed'
    form_class = UserCreationForm
    success_url = 'registration_end'
    template_name = 'registration_form.html'

    def register(self, request, **cleaned_data):
        email, password = cleaned_data['email'], cleaned_data['password1']
        MyUser.objects.create_user(email, password)

        new_user = authenticate(email=email, password=password)
        login(request, new_user)
        return new_user

    def registration_allowed(self, request):
        """
        Indicate whether account registration is currently permitted,
        based on the value of the setting ``REGISTRATION_OPEN``. This
        is determined as follows:

        * If ``REGISTRATION_OPEN`` is not specified in settings, or is
          set to ``True``, registration is permitted.

        * If ``REGISTRATION_OPEN`` is both specified and set to
          ``False``, registration is not permitted.
        
        """
        return getattr(settings, 'REGISTRATION_OPEN', True)

    def dispatch(self, request, *args, **kwargs):
        """
        Check if registration is enabled
        """
        if not self.registration_allowed(request):
            return redirect(self.disallowed_url)

    def get_success_url(self, request, user):
        return (user.get_absolute_url(), (), {})

    def form_valid(self, form):
        """
        A valid form has been POSTed, so we do the registration thing.

        """

        new_user = self.register(self.request, **form.cleaned_data)
        success_url = self.get_success_url(self.request, new_user)

        # success_url may be a simple string, or a tuple providing the
        # full argument set for redirect(). Attempting to unpack it
        # tells us which one it is.
        try:
            to, args, kwargs = success_url
            return redirect(to, *args, **kwargs)
        except ValueError:
            return redirect(success_url)